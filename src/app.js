// Components
import './theme/index.scss';
import MainPages from './components/MainPages/MainPages';
// Instruments

export const App = () => {
    return (
        <main>
            <MainPages />
        </main>
    );
};

