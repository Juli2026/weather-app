import { useEffect } from 'react';
import { useQuery } from 'react-query';
import { api } from '../api/api';
import { useStore } from './useStore';


export const useGetData = () => {
    const { weatherStore } = useStore();
    const { selectedId } = weatherStore;
    const query = useQuery('weather', () => api.getWeather());
    const { data, isFetched } = query;

    useEffect(() => {
        if (!selectedId && Array.isArray(data) && data?.length) {
            weatherStore.setSelectedId(data[ 0 ].day);
        }
    }, [data]);

    return {
        data: Array.isArray(data) ? data : [],
        isFetched,
    };
};
