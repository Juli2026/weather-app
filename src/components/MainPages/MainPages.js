/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../hooks/useStore';
import { useGetData } from '../../hooks/useGetData';
import Forecast  from '../Forecast/Forecast';
import '../../theme/forecast.scss';
import TopPages from '../TopPages/TopPages';
import CurrentWeather from '../Current-weather/CurrentWeather';
import Filter from '../Filter/Filters';

const MainPages = observer(() => {
    const { weatherStore } = useStore();
    const { selectedId, setSelectedId } = weatherStore;
    const { isFiltered } = weatherStore;

    const { data } = useGetData();
    const days = isFiltered ? weatherStore.filteredDays(data) : data;
    const limited = days.filter((val, i) => i < 7);

    const selectedDay =  days.find((day) => day.day === selectedId);

    const handleShow = (day) => {
        if (selectedId !== day) {
            setSelectedId(day);
        }
    };

    return (
        <div>
            <Filter />
            { selectedDay ? <TopPages items = { selectedDay } /> : '' }
            { selectedDay ? <CurrentWeather date = { selectedDay } /> : '' }
            <div className = 'forecast'>
                { limited.length
                    ? limited.map((item, index) => (
                        <Forecast
                            item = { item } key = { index }
                            selected = { selectedId === item.day }
                            onClick = { () => {
                                handleShow(item.day);
                            }
                            } />
                    ))
                    : <p className = 'message'>По заданим параметрам нічого не знайдено</p>
                }

            </div>
        </div>
    );
});

export default MainPages;
