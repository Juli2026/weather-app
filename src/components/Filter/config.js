import * as yup from 'yup';

export const schema = yup.object().shape({
    minTemperature: yup.number(),
    maxTemperature: yup.number(),
});
