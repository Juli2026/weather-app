/* eslint-disable radix */
import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { schema } from './config';

import { useStore } from '../../hooks/useStore';
import '../../theme/filter.scss';
import { Input } from './Element/Input';

const Filter = observer(() => {
    const form  = useForm({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const { weatherStore } = useStore();
    const { type, setType } = weatherStore;
    const {  applyFilter } = weatherStore;
    const { resetFilter, isFiltered } = weatherStore;

    const onReset = () => {
        resetFilter();
        form.reset();
    };

    const onSubmit = form.handleSubmit(async (data) => {
        await applyFilter(data);
    });

    return (
        <div className = 'filter'>
            <form>
                <div className = 'filter-box'>
                    <span
                        className = { `checkbox ${type === 'cloudy' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''} ` }
                        onClick = { () => !isFiltered && setType('cloudy') }> Cloudy </span>
                    <span
                        className = { `checkbox ${type === 'sunny' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''} ` }
                        onClick = { () => !isFiltered && setType('sunny') }> Sunny </span>
                </div>
                <div className = 'filterBox'>
                    <label htmlFor = 'minTemperature' className = 'minBox'>Min</label>
                    <Input
                        register = { form.register('minTemperature') }
                        name = 'minTemperature'
                        disabled = { isFiltered ? true : '' }
                        id = 'minTemperature' />
                </div>
                <div className = 'filterBox'>
                    <label htmlFor = 'maxTemperature'>Max</label>
                    <Input
                        name = 'maxTemperature'
                        id = 'maxTemperature'
                        disabled = { isFiltered ? true : '' }
                        register = { form.register('maxTemperature') } />
                </div>
            </form>
            { isFiltered === false
                ?  <button type = 'submit' onClick = { onSubmit }>Filter</button>
                : <button type = 'submit' onClick = { onReset }>Reset</button>
            }
        </div>
    );
});

export default Filter;
