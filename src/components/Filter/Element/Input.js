export const Input = ({ register, ...props }) => {
    const input = (
        <input
            type = { props.type }
            { ...register }
            { ...props } />
    );

    return (
        <label>
            <div>
                { props.label }{ ' ' }
                <span className = 'error-message'>{ props.error?.message }</span>
            </div>
            { input }
        </label>
    );
};
Input.defaultProps = {
    disabled: false,
    type:     'number',
    tag:      'input',
};
