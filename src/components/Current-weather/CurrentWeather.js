import React from 'react';
import '../../theme/current-weather.scss';

const CurrentWeather = ({ date }) => {
    return (
        <div className = 'current-weather'>
            <p className = 'temperature '>{ date.temperature }</p>
            <p className = 'meta'>
                <span className = 'rainy'>%{ date.rain_probability }</span>
                <span className = 'humidity'>%{ date.humidity }</span>
            </p>
        </div>
    );
};

export default CurrentWeather;
