import React from 'react';
import { format } from 'date-fns';

import '../../theme/head.scss';

const TopPages = ({ items }) => {
    console.log('Top', items);
    const getDayHead = format(new Date(items.day), 'EEEE');
    const myRainyClassHead = items.type === 'rainy' ? 'rainy' : '';
    const myCloudyClassHead = items.type === 'cloudy' ? 'cloudy' : '';
    const mySunnyClassHead = items.type === 'sunny' ? 'sunny' : '';
    const data = format(new Date(items.day), 'd MMMM');

    return (
        <div className = 'head'>
            <span className = { `icon ${myRainyClassHead} ${myCloudyClassHead} ${mySunnyClassHead}` }></span>
            <div className = 'current-date'>
                <p>{ getDayHead }</p>
                <span>{ data }</span>
            </div>
        </div>
    );
};

export default TopPages;
