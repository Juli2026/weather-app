import React from 'react';
import { format } from 'date-fns';
import '../../theme/forecast.scss';


const Forecast = ({ item, selected, onClick }) => {
    const getDay = format(new Date(item.day), 'EEEE');
    const myRainyClass = item.type === 'rainy' ? 'rainy' : '';
    const myCloudyClass = item.type === 'cloudy' ? 'cloudy' : '';
    const mySunnyClass = item.type === 'sunny' ? 'sunny' : '';

    return (
        <div className = { `day ${myRainyClass} ${myCloudyClass} ${mySunnyClass} ${selected ? 'selected' : ''}` } onClick = { onClick } >
            <p>{ getDay }</p>
            <span>{ item.temperature }</span>
        </div>
    );
};

export default Forecast;
